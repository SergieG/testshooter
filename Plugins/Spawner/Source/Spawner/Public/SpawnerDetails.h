﻿#pragma once

#include "CoreMinimal.h"
#include "IDetailCustomization.h"



class FSpawnerDetails : public IDetailCustomization 
{
    
public:

    /** Makes a new instance of this detail layout class for a specific detail view requesting it */
    static TSharedRef<IDetailCustomization> MakeInstance();

    virtual void CustomizeDetails(IDetailLayoutBuilder& DetailBuilder) override;

};    