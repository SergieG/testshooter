// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnerActor.generated.h"

UCLASS()
class SPAWNER_API ASpawnerActor : public AActor
{
	GENERATED_BODY()

public:

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= Debug)
	UPROPERTY()
	TArray<FVector> Points;

	UPROPERTY()
	int32 CurrentSelectedPointIndex = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= SpawnList)
	TArray<TSubclassOf<AActor>> SpawnItems;

	UPROPERTY()
	bool bRandomSpawn = false;

	UPROPERTY()
	float SpawnDelayTime = 1.f;

private:

	UPROPERTY()
	int8 CurrentSpawnIndex = 0;

	UPROPERTY()
	TMap<FVector, FTimerHandle> FreePoints;

	UPROPERTY()
	TMap<FVector, AActor*> OccupiedPoints;

public:
	// Sets default values for this actor's properties
	ASpawnerActor();

protected:
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	UFUNCTION()
    void OnPickup(AActor* MyOverlappedActor, AActor* OtherActor);
	UFUNCTION()
    AActor* Spawn(FVector SpawnPoint);
	UFUNCTION()
    void DelayedSpawn(FVector SpawnPoint);

};
