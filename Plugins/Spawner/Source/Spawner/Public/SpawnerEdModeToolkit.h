// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

//#include "SpawnerActor.h"
//#include "SpawnerDetails.h"
//#include "SpawnerEdMode.h"
#include "SpawnerDetails.h"
#include "SpawnerWidget.h"
#include "Toolkits/BaseToolkit.h"

class FSpawnerEdModeToolkit : public FModeToolkit
{
public:

	FSpawnerEdModeToolkit();
	
	/** FModeToolkit interface */
	virtual void Init(const TSharedPtr<IToolkitHost>& InitToolkitHost) override;

	/** IToolkit interface */
	virtual FName GetToolkitFName() const override;
	virtual FText GetBaseToolkitName() const override;
	virtual class FEdMode* GetEditorMode() const override;
	virtual TSharedPtr<class SWidget> GetInlineContent() const override { return SPWidget; }
	
private:

	TSharedPtr<SSpawnerWidget> SPWidget;
	TSharedPtr<FSpawnerDetails> SpawnerDetails;	
};
