// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "EdMode.h"

//class USpawnerEditorObject;

struct HExamplePointProxy : public HHitProxy
{
	DECLARE_HIT_PROXY();

	HExamplePointProxy(UObject* InRefObject, int32 InIndex)
        : HHitProxy(HPP_UI), RefObject(InRefObject), Index(InIndex)
	{}

	UObject* RefObject;
	int32 Index;
};


class FSpawnerEdMode : public FEdMode
{
public:
	const static FEditorModeID EM_SpawnerEdModeId;

	FSpawnerEdMode();
	virtual ~FSpawnerEdMode();

	// FEdMode interface
	virtual void Enter() override;
	virtual void Exit() override;
	bool UsesToolkits() const override;

	virtual void Render(const FSceneView* View, FViewport* Viewport, FPrimitiveDrawInterface* PDI) override;
	virtual bool HandleClick(FEditorViewportClient* InViewportClient, HHitProxy *HitProxy, const FViewportClick &Click) override;

	virtual bool InputDelta(FEditorViewportClient* InViewportClient, FViewport* InViewport, FVector& InDrag, FRotator& InRot, FVector& InScale) override;
	virtual bool ShowModeWidgets() const override;
	virtual bool ShouldDrawWidget() const override;
	virtual bool UsesTransformWidget() const override;
	virtual FVector GetWidgetLocation() const override;
};
