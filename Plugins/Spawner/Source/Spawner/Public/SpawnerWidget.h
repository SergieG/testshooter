﻿#pragma once

#include "CoreMinimal.h"
#include "Widgets/DeclarativeSyntaxSupport.h"
#include "Widgets/SCompoundWidget.h"

class IDetailsView;
struct FPropertyAndParent;

class FSpawnerEdModeToolkit;
class ITableRow;
class STableViewBase;

class SSpawnerWidget : public SCompoundWidget
{
public:
    
    SLATE_BEGIN_ARGS( SSpawnerWidget ){}
    SLATE_ARGUMENT(const TArray<UObject*>*, Actors)
    SLATE_END_ARGS()

    //void Construct(const FArguments& InArgs, TSharedRef<FSpawnerEdModeToolkit> InParentToolkit);
    void Construct(const FArguments& InArgs);
    
    // void RefreshDetailPanel();

protected:
    
   class FSpawnerEdMode* GetEditorMode() const;
   bool GetSpawnerEditorIsEnabled() const;
   bool GetIsPropertyVisible(const FPropertyAndParent& PropertyAndParent) const;

    //TSharedPtr<IDetailsView> DetailsPanel;

private:

    //TArray< TSharedPtr <FString>> TestStrings;
      
    static bool IsWidgetEnabled();
    ECheckBoxState LoadIsRandomValue() const;
    void OnRandomCheckChanged(ECheckBoxState NewState);
    void DelayValueChanged(float NewValue);
    float LoadDelayValue() const;
    FReply AddPoint();
    FReply RemovePoint();

    //TSharedRef<ITableRow> MakeRow(TSharedPtr <FString> Item, const TSharedRef<STableViewBase>& OwnerTable);

};