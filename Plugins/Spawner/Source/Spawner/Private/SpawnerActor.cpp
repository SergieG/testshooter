// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnerActor.h"
#include "GameFramework/Character.h"

// Sets default values
ASpawnerActor::ASpawnerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetActorHiddenInGame(true);
}

// Called when the game starts or when spawned
void ASpawnerActor::BeginPlay()
{
	Super::BeginPlay();

	if(HasAuthority())
	{
		for (auto Point : Points)
		{
			 AActor* CurrentSpawnedItem = Spawn(Point);

			OccupiedPoints.Add(Point, CurrentSpawnedItem);

			if(CurrentSpawnedItem)
				CurrentSpawnedItem->OnActorBeginOverlap.AddDynamic(this, &ASpawnerActor::OnPickup); 
		}
	}
}

// Called every frame
void ASpawnerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpawnerActor::OnPickup(AActor* MyOverlappedActor, AActor* OtherActor)
{
	if(HasAuthority())
	{
		if(OtherActor && Cast<ACharacter>(OtherActor))
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("OnPickUp"));

			const FVector* PickUpPoint = OccupiedPoints.FindKey(MyOverlappedActor);
			FTimerHandle TimerHandle;
			FTimerDelegate TimerDel = FTimerDelegate::CreateUObject( this, &ASpawnerActor::DelayedSpawn, *PickUpPoint );
			//TimerDel.BindUFunction(this, FName("DelayedSpawn"), PickUpPoint);
			//FTimerHandle CurrentTimer = GetWorld()->GetTimerManager().GenerateHandle(FreePoints.Num());
			//GetWorld()->GetTimerManager().SetTimer(CurrentTimer, this, &ASpawnerActor::DelayedSpawn, SpawnDelayTime, false);
			//GetWorld()->GetTimerManager().SetTimer(CurrentTimer, this, TimerDel, SpawnDelayTime, false);
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, SpawnDelayTime, false);
			FreePoints.Add(*PickUpPoint, TimerHandle);
			OccupiedPoints.Remove(*PickUpPoint);
			MyOverlappedActor->Destroy();
		}
	}
}

AActor* ASpawnerActor::Spawn(FVector SpawnPoint)
{
	if(GetWorld() && SpawnItems.Num() > 0)
	{
		if(bRandomSpawn)
			CurrentSpawnIndex = FMath::RandRange(0, SpawnItems.Num() - 1);
				
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		AActor* SpawnedItem =GetWorld()->SpawnActor<AActor>(SpawnItems[CurrentSpawnIndex], SpawnPoint, FRotator::ZeroRotator, SpawnInfo);

		if(!bRandomSpawn)
		{
			CurrentSpawnIndex++;

			if(CurrentSpawnIndex > SpawnItems.Num() - 1)
				CurrentSpawnIndex = 0;
		}
			
		return SpawnedItem;
	}
		
	return  nullptr;
}

void ASpawnerActor::DelayedSpawn(FVector SpawnPoint)
{
	if(FreePoints.Num() > 0)
	{
		AActor* CurrentSpawnedItem = Spawn(SpawnPoint);
		OccupiedPoints.Add(SpawnPoint, CurrentSpawnedItem);
		FreePoints.Remove(SpawnPoint);

		if(CurrentSpawnedItem)
			CurrentSpawnedItem->OnActorBeginOverlap.AddDynamic(this, &ASpawnerActor::OnPickup);
	}
}

