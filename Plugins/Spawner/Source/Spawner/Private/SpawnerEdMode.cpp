// Copyright Epic Games, Inc. All Rights Reserved.

#include "SpawnerEdMode.h"
#include "SpawnerEdModeToolkit.h"
#include "Toolkits/ToolkitManager.h"
#include "EditorModeManager.h"
#include "EngineUtils.h"
#include "SpawnerActor.h"
#include "Engine/Selection.h"

IMPLEMENT_HIT_PROXY(HExamplePointProxy, HHitProxy);

const FEditorModeID FSpawnerEdMode::EM_SpawnerEdModeId = TEXT("EM_SpawnerEdMode");

FSpawnerEdMode::FSpawnerEdMode()
{

}

FSpawnerEdMode::~FSpawnerEdMode()
{

}

void FSpawnerEdMode::Enter()
{
	FEdMode::Enter();

	if (!Toolkit.IsValid() && UsesToolkits())
	{
		Toolkit = MakeShareable(new FSpawnerEdModeToolkit);
		Toolkit->Init(Owner->GetToolkitHost());
	}
}

void FSpawnerEdMode::Exit()
{
	// Call base Exit method to ensure proper cleanup
	FEdMode::Exit();

	if (Toolkit.IsValid())
	{
		FToolkitManager::Get().CloseToolkit(Toolkit.ToSharedRef());
		Toolkit.Reset();
	}


}

bool FSpawnerEdMode::UsesToolkits() const
{
	return true;
}


void FSpawnerEdMode::Render(const FSceneView* View, FViewport* Viewport, FPrimitiveDrawInterface* PDI)
{
	FEdMode::Render(View, Viewport, PDI);
	
	UWorld* World = GetWorld();
	for (TActorIterator<ASpawnerActor> It(World); It; ++It)
	{
		ASpawnerActor* CurrentSpawner = (*It);
		if (CurrentSpawner)
		{
			for (int i = 0; i < CurrentSpawner->Points.Num(); ++i)
			{		
				const FColor color(139, 241, 139); 
				
				// set hit proxy and draw
				PDI->SetHitProxy(new HExamplePointProxy(CurrentSpawner, i));
				PDI->DrawPoint(CurrentSpawner->Points[i], color, 15.f, SDPG_Foreground);
				PDI->DrawLine(CurrentSpawner->Points[i], CurrentSpawner->GetActorLocation(), color, SDPG_Foreground);
				PDI->SetHitProxy(NULL);
			}
		}
	}
}

bool FSpawnerEdMode::HandleClick(FEditorViewportClient* InViewportClient, HHitProxy* HitProxy,
	const FViewportClick& Click)
{
	bool isHandled = false;

	if (HitProxy)
	{
		if (HitProxy->IsA(HExamplePointProxy::StaticGetType()))
		{
			isHandled = true;
			HExamplePointProxy* examplePointProxy = (HExamplePointProxy*)HitProxy;
			ASpawnerActor* Spawner = Cast<ASpawnerActor>(examplePointProxy->RefObject);
			int32 index = examplePointProxy->Index;

			if (Spawner && index >= 0 && index < Spawner->Points.Num())
			{
				Spawner->CurrentSelectedPointIndex = index;
			}
		}
	}

	return isHandled;
}

bool FSpawnerEdMode::InputDelta(FEditorViewportClient* InViewportClient, FViewport* InViewport, FVector& InDrag,
	FRotator& InRot, FVector& InScale)
{
	if (InViewportClient->GetCurrentWidgetAxis() == EAxisList::None)
	{
		return false;
	}

	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner && GEditor->GetSelectedActors()->Num() == 1 && Spawner->Points.Num() > 0)
	{
		if (!InDrag.IsZero())
		{
			Spawner->Modify();
			Spawner->Points[Spawner->CurrentSelectedPointIndex] += InDrag;
		}

		return true;
	}

	return false;
}

bool FSpawnerEdMode::ShowModeWidgets() const
{
	return true;
}

bool FSpawnerEdMode::ShouldDrawWidget() const
{
	return true;
}

bool FSpawnerEdMode::UsesTransformWidget() const
{
	return true;
}

FVector FSpawnerEdMode::GetWidgetLocation() const
{
	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner && GEditor->GetSelectedActors()->Num() == 1 && Spawner->Points.Num() > 0)
		return Spawner->Points[Spawner->CurrentSelectedPointIndex];

	return FEdMode::GetWidgetLocation();
}




