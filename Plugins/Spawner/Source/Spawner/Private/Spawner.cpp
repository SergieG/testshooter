// Copyright Epic Games, Inc. All Rights Reserved.

#include "Spawner.h"


#include "SpawnerActor.h"
#include "SpawnerDetails.h"
#include "SpawnerEdMode.h"

#define LOCTEXT_NAMESPACE "FSpawnerModule"

void FSpawnerModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	FEditorModeRegistry::Get().RegisterMode<FSpawnerEdMode>(FSpawnerEdMode::EM_SpawnerEdModeId, LOCTEXT("SpawnerEdModeName", "SpawnerEdMode"), FSlateIcon(), true);
	FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.RegisterCustomClassLayout(ASpawnerActor::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&FSpawnerDetails::MakeInstance));
}

void FSpawnerModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	FEditorModeRegistry::Get().UnregisterMode(FSpawnerEdMode::EM_SpawnerEdModeId);

	FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.UnregisterCustomClassLayout(ASpawnerActor::StaticClass()->GetFName());
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FSpawnerModule, Spawner)