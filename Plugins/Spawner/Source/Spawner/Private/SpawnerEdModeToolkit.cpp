// Copyright Epic Games, Inc. All Rights Reserved.

#include "SpawnerEdModeToolkit.h"

#include "SpawnerEdMode.h"
#include "Engine/Selection.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Text/STextBlock.h"
#include "EditorModeManager.h"
#include "SpawnerActor.h"
#include "Widgets/Input/SSpinBox.h"

#define LOCTEXT_NAMESPACE "FSpawnerEdModeToolkit"

FSpawnerEdModeToolkit::FSpawnerEdModeToolkit()
{

}

void FSpawnerEdModeToolkit::Init(const TSharedPtr<IToolkitHost>& InitToolkitHost)
{
	SAssignNew(SPWidget, SSpawnerWidget);
	SpawnerDetails = StaticCastSharedRef<FSpawnerDetails>(FSpawnerDetails::MakeInstance());

	FModeToolkit::Init(InitToolkitHost);
	
}

FName FSpawnerEdModeToolkit::GetToolkitFName() const
{
	return FName("SpawnerEdMode");
}

FText FSpawnerEdModeToolkit::GetBaseToolkitName() const
{
	return NSLOCTEXT("SpawnerEdModeToolkit", "DisplayName", "SpawnerEdMode Tool");
}

class FEdMode* FSpawnerEdModeToolkit::GetEditorMode() const
{
	return GLevelEditorModeTools().GetActiveMode(FSpawnerEdMode::EM_SpawnerEdModeId);
}

#undef LOCTEXT_NAMESPACE
