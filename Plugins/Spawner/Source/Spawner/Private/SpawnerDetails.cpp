﻿
#include "SpawnerDetails.h"

#include "DetailCategoryBuilder.h"
#include "DetailLayoutBuilder.h"
#include "DetailWidgetRow.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Input/SButton.h"
#include "Widgets/Input/SSpinBox.h"
#include "Widgets/Input/SCheckBox.h"

//class IDetailCategoryBuilder;

TSharedRef<IDetailCustomization> FSpawnerDetails::MakeInstance()
{
    return MakeShareable(new FSpawnerDetails);
}

void FSpawnerDetails::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
    
}
