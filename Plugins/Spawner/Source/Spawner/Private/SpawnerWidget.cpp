﻿#include "SpawnerWidget.h"
#include "EditorModeManager.h"
#include "SpawnerEdMode.h"
#include "Widgets/Layout/SScrollBox.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Input/SCheckBox.h"
#include "Widgets/Input/SSpinBox.h"
#include "Widgets/Layout/SBorder.h"
#include "Engine/Selection.h"
//#include "Widgets/Input/SVectorInputBox.h"
#include "Kismet/GameplayStatics.h"
#include "SpawnerActor.h"
//#include "Widgets/Layout/SExpandableArea.h"
//#include "Widgets/Views/SListView.h"


void SSpawnerWidget::Construct(const FArguments& InArgs)
{
    /*FPropertyEditorModule& PropertyEditorModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
    FDetailsViewArgs DetailsViewArgs(false, false, false,FDetailsViewArgs::HideNameArea);

    DetailsPanel = PropertyEditorModule.CreateDetailView(DetailsViewArgs);
    DetailsPanel->SetIsPropertyVisibleDelegate(FIsPropertyVisible::CreateSP(this, &SSpawnerWidget::GetIsPropertyVisible));

    USelection::SelectObjectEvent.AddRaw(this, &SSpawnerWidget::OnObjectSelected);

    ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

    if(Spawner)
        DetailsPanel->SetObject(Spawner);*/
	

    ChildSlot
    [
		 SNew(SBorder)
        .HAlign(HAlign_Center)
        .Padding(15)
        .IsEnabled_Static(&SSpawnerWidget::IsWidgetEnabled)
		//.
        [
            SNew(SVerticalBox)
            + SVerticalBox::Slot()
            .HAlign(HAlign_Center)
            .AutoHeight()
            .Padding(10)
			[
                SNew(SHorizontalBox)
                + SHorizontalBox::Slot()
               .AutoWidth()
				[
	               SNew(STextBlock)
	               .AutoWrapText(true)
	               .Text(FText::FromString("Select Spawner Actor"))
				]
			]
			
			+ SVerticalBox::Slot()
           .HAlign(HAlign_Center)
           .AutoHeight()
           .Padding(10)
           [
				SNew(SCheckBox)
				//.OnCheckStateChanged(this, &FSpawnerEdModeToolkit::CheckUnchek)
				//.IsChecked_Static(&SSpawnerWidget::CheckIsRandom)
				.IsChecked(this, &SSpawnerWidget::LoadIsRandomValue)
				.OnCheckStateChanged(this, &SSpawnerWidget::OnRandomCheckChanged)
	              [ SNew(STextBlock)
	                  .AutoWrapText(true)
	                  .Text(FText::FromString("Random Spawn"))
	              ]
           ]

           + SVerticalBox::Slot()
           .HAlign(HAlign_Center)
           .AutoHeight()
           .Padding(10)
           [
	           SNew(SHorizontalBox)
	          + SHorizontalBox::Slot()
	          .AutoWidth()
	          [
		          SNew(SSpinBox<float>)
					//.Value(1.f)
					.Value(this, &SSpawnerWidget::LoadDelayValue)
					//.Value(GetSpawnerDelay())
	                .MinValue(0.f)
	                .MaxValue(10000.f)
	                .MinSliderValue(0.f)
	                .MaxSliderValue(10000.f)
	                .MinFractionalDigits(1)
	                .MaxFractionalDigits(2)
	                .OnValueChanged(this, &SSpawnerWidget::DelayValueChanged)
	          ]
	          + SHorizontalBox::Slot()
              .AutoWidth()
              .VAlign(VAlign_Center)
              .Padding(5)
                  [ SNew(STextBlock)
                      .AutoWrapText(true)
                      .Text(FText::FromString("Spawn Delay"))
                  ]
           ]

           + SVerticalBox::Slot()
           .HAlign(HAlign_Center)
           .AutoHeight()
           .Padding(10)
           [
              SNew(STextBlock)
                  .AutoWrapText(true)
                  //.Text(LOCTEXT("HelperLabel", "Spawn Points"))
                  .Text(FText::FromString("Spawn Points"))
           ]
     
	        + SVerticalBox::Slot()
	        .HAlign(HAlign_Center)
	        .AutoHeight()
	        .Padding(10)
	        [
	            SNew(SHorizontalBox)
	            + SHorizontalBox::Slot()
	            .AutoWidth()
	            [
	                SNew(SButton)
	                    .Text(FText::FromString("Add"))
	                    //.OnClicked_Static()
	                    .OnClicked(this, &SSpawnerWidget::AddPoint)
	            ]
	        
	            + SHorizontalBox::Slot()
	            .AutoWidth()
	            [
	                SNew(SButton)
	                    .Text(FText::FromString("Remove"))
	                    //.OnClicked_Static()
	                    .OnClicked(this, &SSpawnerWidget::RemovePoint)
	            ]
	        ]
	        /*+ SVerticalBox::Slot()
	       .HAlign(HAlign_Center)
	       .AutoHeight()
	       .Padding(10)
	       [
	           SNew(SVectorInputBox)
	           .bColorAxisLabels(true)
	           .AllowSpin(true)
	       ]*/
	       /*+ SVerticalBox::Slot()
           .HAlign(HAlign_Center)
           .AutoHeight()
           .Padding(10)
			[
	             SNew(SListView<UObject*>)
	             .ItemHeight(24)
	             .SelectionMode(ESelectionMode::None)
	             .ListItemsSource(&ActorsAsObjects)
	             .OnGenerateRow(this, &SSpawnerWidget::MakeRow)
	             .Visibility(EVisibility::Visible)
			]*/

	       /*+ SVerticalBox::Slot()
          .Padding(0)
          [
			DetailsPanel.ToSharedRef()
          ]*/
			
		]
    ];

	/*TestStrings.Add(MakeShareable(new FString("Hello")));
	TestStrings.Add(MakeShareable(new FString("World")));

	ChildSlot
    [
        SNew(SExpandableArea)
        .InitiallyCollapsed(false)
        .Padding(0.0f)
        .MaxHeight(150.0f)
        .AreaTitle(NSLOCTEXT("SCapturedActorsWidget", "SelectedActorsText", "Captured Actors"))
        .BodyContent()
        [
            SNew(SListView<TSharedPtr <FString>>)
            .ItemHeight(24)
            //.SelectionMode(ESelectionMode::None)
            .ListItemsSource(&TestStrings)
            .OnGenerateRow(this, &SSpawnerWidget::MakeRow)
            //.Visibility(EVisibility::Visible)
        ]
    ];*/

}

FSpawnerEdMode* SSpawnerWidget::GetEditorMode() const
{
    return (FSpawnerEdMode*)GLevelEditorModeTools().GetActiveMode(FSpawnerEdMode::EM_SpawnerEdModeId);
}

bool SSpawnerWidget::GetSpawnerEditorIsEnabled() const
{
    return  true;
}

bool SSpawnerWidget::GetIsPropertyVisible(const FPropertyAndParent& PropertyAndParent) const
{
    return true;
}

bool SSpawnerWidget::IsWidgetEnabled()
{
	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner &&  GEditor->GetSelectedActors()->Num() == 1)
		return  true;
	
	return false;
}

ECheckBoxState SSpawnerWidget::LoadIsRandomValue() const
{
	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner)
	{
		if(Spawner->bRandomSpawn)
			return ECheckBoxState::Checked;
	}

	return ECheckBoxState::Unchecked;
}

void SSpawnerWidget::OnRandomCheckChanged(ECheckBoxState NewState)
{	
	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner)
	{
		if(NewState == ECheckBoxState::Checked)
			Spawner->bRandomSpawn = true;
		if(NewState == ECheckBoxState::Unchecked)
			Spawner->bRandomSpawn = false;
	}
}

void SSpawnerWidget::DelayValueChanged(float NewValue)
{
	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner)
		Spawner->SpawnDelayTime = NewValue;
}

float SSpawnerWidget::LoadDelayValue() const
{
	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner)
		return Spawner->SpawnDelayTime;

	return 1.f;
}

FReply SSpawnerWidget::AddPoint()
{
	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner)
	{
		FVector Point = Spawner->GetActorLocation();
		Spawner->Points.Add(Point);
		Spawner->CurrentSelectedPointIndex = Spawner->Points.Num() -1;
	}

	return FReply::Handled();
}

FReply SSpawnerWidget::RemovePoint()
{
	ASpawnerActor* Spawner = Cast<ASpawnerActor>(GEditor->GetSelectedActors()->GetSelectedObject(0));

	if(Spawner && Spawner->Points.Num() > 0)
	{
		Spawner->Points.RemoveAt(Spawner->CurrentSelectedPointIndex);
		Spawner->CurrentSelectedPointIndex = 0;
	}

	return FReply::Handled();
}

/*TSharedRef<ITableRow> SSpawnerWidget::MakeRow(TSharedPtr <FString> Item, const TSharedRef<STableViewBase>& OwnerTable)
{
	return
	    SNew(STableRow<TSharedPtr <FString>>, OwnerTable)
	    /*[
	        SNew(SHorizontalBox)
	        + SHorizontalBox::Slot()
	        .Padding(0.0f, 2.0f, 2.0f, 4.0f)
	        .FillWidth(1.0)
	        .VAlign(VAlign_Bottom)
	        .HAlign(HAlign_Left)#1#
	        [
	            SNew(STextBlock)
	            .Text(FText::FromString(*Item.Get()))
	        ];
		//];	
}*/

