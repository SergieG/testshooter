// Fill out your copyright notice in the Description page of Project Settings.


#include "TestShooterWeaponBase.h"


#include "DrawDebugHelpers.h"
#include "TestShooterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATestShooterWeaponBase::ATestShooterWeaponBase()
{
	SetReplicates(true);

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = WeaponMesh;
	WeaponMesh->SetCollisionProfileName("Weapon");
	WeaponMesh->CanCharacterStepUpOn = ECB_No;

	/*
	Muzzle = CreateDefaultSubobject<USceneComponent>(TEXT("Muzzle"));
	Muzzle->SetupAttachment(WeaponMesh);
	*/
	
	//WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	//WeaponMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ATestShooterWeaponBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATestShooterWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

/*FVector ATestShooterWeaponBase::GetAimPosition()
{
	return AimPosition;
}*/

void ATestShooterWeaponBase::ShootServer_Implementation()
{
	ATestShooterCharacter* CharacterOwner = Cast<ATestShooterCharacter>(GetOwner());

	if (ProjectileClass && CharacterOwner)
	{
		FHitResult OutHit = FHitResult(ForceInit);
		const FVector Start = CharacterOwner->Camera->GetComponentLocation();
		FVector End = (UKismetMathLibrary::GetForwardVector(CharacterOwner->Camera->GetComponentRotation()) * 100000.f) + Start;

		bool bIsHit = GetWorld()->LineTraceSingleByChannel(
          OutHit,      // FHitResult object that will be populated with hit info
          Start,      // starting position
          End,        // end position
          ECC_Visibility  // collision channel - 3rd custom one
    );

		if(bIsHit)
		{
			AimPosition = OutHit.ImpactPoint;
			//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 0.f, ECC_WorldStatic, 1.f);
		
		}
		else
		{
			AimPosition = OutHit.TraceEnd;
		}


		const FVector SpawnLocation = WeaponMesh->GetSocketLocation("Muzzle");
		//const FVector SpawnLocation = Muzzle->GetComponentLocation();
		const FRotator SpawnRotation = UKismetMathLibrary::FindLookAtRotation(SpawnLocation, AimPosition);

		//Set Spawn Collision Handling Override
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ActorSpawnParams.Instigator = CharacterOwner->GetInstigator();
		ActorSpawnParams.Owner = CharacterOwner;
	
		//ShootMulticast();

		if(GetWorld())
			GetWorld()->SpawnActor<ATestShooterProjectileBase>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		
	}
}

/*void ATestShooterWeaponBase::ShootServer(AController* Controller)
{
	
}*/

/*void ATestShooterWeaponBase::ShootMulticast_Implementation()
{
	//Set Spawn Collision Handling Override
	/*FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	// spawn the projectile at the muzzle
	if(GetWorld())
		GetWorld()->SpawnActor<ATestShooterProjectileBase>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);#1#

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("ShootMulticast"));
}*/

                                                                                                                                                                                 