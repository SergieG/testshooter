// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "TestShooterSpawnItem.h"
#include "GameFramework/Actor.h"
#include "TestShooterSpawnerPoint.generated.h"

UCLASS()
class TESTSHOOTER_API ATestShooterSpawnerPoint : public AActor
{
	GENERATED_BODY()

	UPROPERTY()
	ATestShooterSpawnItem* CurrentSpawnedItem;

	UPROPERTY()
	int8 CurrentSpawnIndex = 0;
	
protected:

	UPROPERTY(VisibleDefaultsOnly)
	class UStaticMeshComponent* SpawnPointMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= SpawnPoint)
	TArray<TSubclassOf<ATestShooterSpawnItem>> SpawnItems;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= SpawnPoint)
	bool bRandomSpawn = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= SpawnPoint)
	float SpawnDelayTime = 1.f;

	/* Handle to manage the timer */
	FTimerHandle PickUpDelayTimerHandle;
	
public:
	// Sets default values for this actor's properties
	ATestShooterSpawnerPoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	
	UFUNCTION()
    void OnPickup(AActor* MyOverlappedActor, AActor* OtherActor);
	UFUNCTION()
	ATestShooterSpawnItem* Spawn();
	UFUNCTION()
    void SpawnDelay();
	
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
