// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"

#include "TestShooterCharacter.generated.h"

UCLASS()
class TESTSHOOTER_API ATestShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	UPROPERTY(VisibleDefaultsOnly, Category = Camera)
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	UCameraComponent* Camera;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = Battle)
	bool bIsAiming = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Battle)
	float AimingWalkSpeed = 250.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= Battle)
	TSubclassOf<class ATestShooterWeaponBase> WeaponClass;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = Battle)
	float AimOffsetPitch;

private:
	float OriginalWalkSpeed;

protected:

	UPROPERTY(Replicated, BlueprintReadOnly, Category = Battle)
	class ATestShooterWeaponBase* EquippedWeapon;

	/*protected:
	UPROPERTY(BlueprintReadOnly, Category = Battle)
	class ATestShooterWeaponBase* EquippedWeapon;*/

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	
	// Sets default values for this character's properties
	ATestShooterCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void GetLifetimeReplicatedProps(::TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	class ATestShooterWeaponBase* GetEquippedWeapon();
	
	UFUNCTION()
	void MoveForwardBackward(float Value);
	UFUNCTION()
	void MoveRightLeft(float Value);
	UFUNCTION(Server, Reliable)
	void StartAimServer();
	UFUNCTION(NetMulticast, Reliable)
    void StartAimMulticast();
	UFUNCTION(Server, Reliable)
	void StopAimServer();
	UFUNCTION(NetMulticast, Reliable)
    void StopAimMulticast();
	UFUNCTION(Server, UnReliable)
	void SetAimOffsetServer();
	/*UFUNCTION()
	void OnRep_AimOffsetPitch();*/

};
