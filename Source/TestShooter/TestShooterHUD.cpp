// Fill out your copyright notice in the Description page of Project Settings.


#include "TestShooterHUD.h"


#include "TestShooterCharacter.h"
#include "TestShooterWeaponBase.h"
#include "Engine/Canvas.h"
#include "Kismet/GameplayStatics.h"

void ATestShooterHUD::DrawHUD()
{
    Super::DrawHUD();

    if (CrosshairTexture)
    {
        // APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
        //
        // if(PlayerController)
        // {
        //     ATestShooterCharacter* Character = Cast<ATestShooterCharacter>(PlayerController->GetCharacter());
        //     
        //     if(Character)
        //     {
        //         FVector AimWorldPosition = Character->GetEquippedWeapon()->GetAimPosition();
        //         FVector2D AimScreenPosition;
        //
        //         PlayerController->ProjectWorldLocationToScreen(AimWorldPosition, AimScreenPosition, false);
        //
        //         // Offset by half of the texture's dimensions so that the center of the texture aligns with the center of the Canvas.
        //         FVector2D CrossHairDrawPosition(AimScreenPosition.X - (CrosshairTexture->GetSurfaceWidth() * 0.5f), AimScreenPosition.Y - (CrosshairTexture->GetSurfaceHeight() * 0.5f));
        //         FCanvasTileItem TileItem(CrossHairDrawPosition, CrosshairTexture->Resource, FLinearColor::White);
        //         TileItem.BlendMode = SE_BLEND_Translucent;
        //         Canvas->DrawItem(TileItem);
        //     }    
        // }

        // find center of the Canvas
        const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

        // offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
        const FVector2D CrosshairDrawPosition( (Center.X -20.0f),(Center.Y) - 20.0f);

        // draw the crosshair
        FCanvasTileItem TileItem(CrosshairDrawPosition, CrosshairTexture->Resource, FLinearColor::White);
        TileItem.BlendMode = SE_BLEND_Translucent;
        Canvas->DrawItem( TileItem );
    }
}
