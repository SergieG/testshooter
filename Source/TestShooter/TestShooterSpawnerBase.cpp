// Fill out your copyright notice in the Description page of Project Settings.


#include "TestShooterSpawnerBase.h"

// Sets default values
ATestShooterSpawnerBase::ATestShooterSpawnerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;
	SpawnerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnerMesh"));
	SpawnerMesh->SetupAttachment(SceneRoot);
	SpawnerMesh->SetCollisionProfileName("BlackAll");

}

// Called when the game starts or when spawned
void ATestShooterSpawnerBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATestShooterSpawnerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

