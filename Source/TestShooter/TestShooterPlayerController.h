// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "TestShooterCharacter.h"
#include "GameFramework/PlayerController.h"
#include "TestShooterPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TESTSHOOTER_API ATestShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadOnly, Category = Player)
	ATestShooterCharacter* PossessedCharacter;
	
public:

	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;

protected:
	
	UFUNCTION()
    void MoveForwardBackward(float Value);
	UFUNCTION()
    void MoveRightLeft(float Value);
	UFUNCTION()
    void StartCrouch();
	UFUNCTION()
    void StopCrouch();
	UFUNCTION()
    void StartAim();
	UFUNCTION()
    void StopAim();
	UFUNCTION()
    void Fire();
	UFUNCTION()
	void Jump();
	UFUNCTION()
	void StopJumping();
	UFUNCTION()
	void Turn(float Val);
	UFUNCTION()
	void LookUp(float Val);
	
};

