// Fill out your copyright notice in the Description page of Project Settings.


#include "TestShooterProjectileBase.h"


#include "TestShooterCharacter.h"
#include "TestShooterPlayerController.h"
#include "Components/SphereComponent.h"
#include "Engine/CollisionProfile.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
ATestShooterProjectileBase::ATestShooterProjectileBase()
{
	SetReplicates(true);
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ATestShooterProjectileBase::OnHit);

	RootComponent = CollisionComp;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
	ProjectileMesh->SetupAttachment(CollisionComp);
	//ProjectileMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ProjectileMesh->SetCollisionProfileName("NoCollision");

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 20000.f;
	ProjectileMovement->MaxSpeed = 20000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
}

/*// Called when the game starts or when spawned
void ATestShooterProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATestShooterProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}*/

void ATestShooterProjectileBase::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if(HasAuthority())
	{
		if(OtherActor && Cast<ATestShooterCharacter>(OtherActor))
		{
			ATestShooterCharacter* HitCharacter = Cast<ATestShooterCharacter>(OtherActor);

			if(HitCharacter)
			{
				ATestShooterPlayerController* PController = Cast<ATestShooterPlayerController>( HitCharacter->GetController());

				if(PController)
				{
					int32 ID = PController->GetPlayerState<APlayerState>()->GetPlayerId();

					if(ID)
					{
						GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Hit Player with ID").Append(FString::FromInt(ID)));
						Destroy(true, false);
					}
				}
			}	
		}
	}

	if(CollisionComp)
		CollisionComp->SetCollisionProfileName("NoCollision");
}

