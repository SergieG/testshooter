// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestShooterProjectileBase.generated.h"

UCLASS()
class TESTSHOOTER_API ATestShooterProjectileBase : public AActor
{
	GENERATED_BODY()

protected:
	
UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
class USphereComponent* CollisionComp;

UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
class UStaticMeshComponent* ProjectileMesh;

UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
class UProjectileMovementComponent* ProjectileMovement;

public:	
	// Sets default values for this actor's properties
	ATestShooterProjectileBase();

	/** called when projectile hits something */
	UFUNCTION()
    void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

};
