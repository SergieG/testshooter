// Fill out your copyright notice in the Description page of Project Settings.


#include "TestShooterSpawnerPoint.h"
#include "TestShooterCharacter.h"


// Sets default values
ATestShooterSpawnerPoint::ATestShooterSpawnerPoint()
{
	SetReplicates(true);
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnPointMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnPointMesh"));
	RootComponent = SpawnPointMesh;
	SpawnPointMesh->SetCollisionProfileName("NoCollision");
	SetActorHiddenInGame(true);
}

// Called when the game starts or when spawned
void ATestShooterSpawnerPoint::BeginPlay()
{
	Super::BeginPlay();

	if(HasAuthority())
	{
		CurrentSpawnedItem = Spawn();

		if(CurrentSpawnedItem)
			//CurrentSpawnedItem->OnDestroyed.AddDynamic(this, &ATestShooterSpawnerPoint::OnPickup);
			CurrentSpawnedItem->OnActorBeginOverlap.AddDynamic(this, &ATestShooterSpawnerPoint::OnPickup);
	}
}

void ATestShooterSpawnerPoint::OnPickup(AActor* MyOverlappedActor, AActor* OtherActor)
{
	if(HasAuthority())
	{
		if(OtherActor && Cast<ATestShooterCharacter>(OtherActor))
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("OnPickUp"));
			if(CurrentSpawnedItem)
				CurrentSpawnedItem->Destroy();
			GetWorld()->GetTimerManager().SetTimer(PickUpDelayTimerHandle, this, &ATestShooterSpawnerPoint::SpawnDelay, SpawnDelayTime, false);
		}
	}
}


ATestShooterSpawnItem* ATestShooterSpawnerPoint::Spawn()
{
	if(GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);

		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::FromInt(SpawnItems.Num()));

		if(bRandomSpawn)
			CurrentSpawnIndex = FMath::RandRange(0, SpawnItems.Num() - 1);
				
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ATestShooterSpawnItem* SpawnedItem =GetWorld()->SpawnActor<ATestShooterSpawnItem>(SpawnItems[CurrentSpawnIndex], GetActorLocation(), GetActorRotation(), SpawnInfo);

		if(!bRandomSpawn)
		{
			CurrentSpawnIndex++;

			if(CurrentSpawnIndex > SpawnItems.Num() - 1)
				CurrentSpawnIndex = 0;
		}
			
		return SpawnedItem;
	}
		
	return  nullptr;
	
	//CurrentSpawnedItem->OnDestroyed.AddDynamic(this, &ATestShooterSpawnerPoint::OnPickup);

}

void ATestShooterSpawnerPoint::SpawnDelay()
{
	CurrentSpawnedItem = Spawn();

	if(CurrentSpawnedItem)
		CurrentSpawnedItem->OnActorBeginOverlap.AddDynamic(this, &ATestShooterSpawnerPoint::OnPickup);

		//Reset collision to take item immediately
		//CurrentSpawnedItem->SetActorEnableCollision(false);
		//CurrentSpawnedItem->SetActorEnableCollision(true);
}

// Called every frame
void ATestShooterSpawnerPoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

