// Fill out your copyright notice in the Description page of Project Settings.


#include "TestShooterCharacter.h"


#include "TestShooterWeaponBase.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ATestShooterCharacter::ATestShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller
	CameraBoom->TargetArmLength = 150.f;
	CameraBoom->SocketOffset = FVector(0, 60.f, 70.f);
	CameraBoom->bEnableCameraLag = true;
	CameraBoom->CameraLagSpeed = 30.f;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);

	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;
	GetCharacterMovement()->CrouchedHalfHeight = 70.f;
	GetCharacterMovement()->MaxWalkSpeedCrouched = 250.f;
	OriginalWalkSpeed = GetCharacterMovement()->MaxWalkSpeed;
}

// Called when the game starts or when spawned
void ATestShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	if(HasAuthority())
	{
		if(WeaponClass)
			EquippedWeapon = GetWorld()->SpawnActor<ATestShooterWeaponBase>(WeaponClass, GetActorLocation(), GetActorRotation());

		if(EquippedWeapon)
		{
			EquippedWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
			//EquippedWeapon->SetOwnerCharacter(this);
			EquippedWeapon->SetOwner(this);
		}
	}

	/*if(IsLocallyControlled())
		EquipWeaponServer();*/
}

// Called every frame
void ATestShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

void ATestShooterCharacter::GetLifetimeReplicatedProps(::TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATestShooterCharacter, EquippedWeapon);
	DOREPLIFETIME(ATestShooterCharacter, AimOffsetPitch);
	DOREPLIFETIME(ATestShooterCharacter, bIsAiming);
}

ATestShooterWeaponBase* ATestShooterCharacter::GetEquippedWeapon()
{
	return EquippedWeapon;
}

void ATestShooterCharacter::MoveForwardBackward(float Value)
{
	if (Controller)
	{
		const FRotator YawRotation(0, Controller->GetControlRotation().Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);	
	}
}

void ATestShooterCharacter::MoveRightLeft(float Value)
{
	if (Controller)
	{
		const FRotator YawRotation(0, Controller->GetControlRotation().Yaw, 0);
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void ATestShooterCharacter::StartAimServer_Implementation()
{
	bIsAiming = true;
	StartAimMulticast();

	/*bIsAiming = true;
	GetCharacterMovement()->MaxWalkSpeed = AimingWalkSpeed;*/
}

void ATestShooterCharacter::StartAimMulticast_Implementation()
{
	bIsAiming = true;
	GetCharacterMovement()->MaxWalkSpeed = AimingWalkSpeed;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("StartAim"));
}

void ATestShooterCharacter::StopAimServer_Implementation()
{
	bIsAiming = false;
	StopAimMulticast();
	/*bIsAiming = false;
	GetCharacterMovement()->MaxWalkSpeed = OriginalWalkSpeed;*/
}

void ATestShooterCharacter::StopAimMulticast_Implementation()
{
	bIsAiming = false;
	GetCharacterMovement()->MaxWalkSpeed = OriginalWalkSpeed;
}

void ATestShooterCharacter::SetAimOffsetServer_Implementation()
{
	AimOffsetPitch =  UKismetMathLibrary::NormalizedDeltaRotator(GetControlRotation(), GetActorRotation()).Pitch;   
}

/*void ATestShooterCharacter::OnRep_AimOffsetPitch()
{
		
}*/

