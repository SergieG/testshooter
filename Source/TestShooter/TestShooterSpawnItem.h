// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestShooterSpawnItem.generated.h"

UCLASS()
class TESTSHOOTER_API ATestShooterSpawnItem : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly)
	class USceneComponent* ItemSceneRoot;
	
	UFUNCTION()
	void PickUp(AActor* MyOverlappedActor, AActor* OtherActor);
	
public:	
	// Sets default values for this actor's properties
	ATestShooterSpawnItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
