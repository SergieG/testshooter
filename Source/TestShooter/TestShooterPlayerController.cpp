// Fill out your copyright notice in the Description page of Project Settings.


#include "TestShooterPlayerController.h"

#include "TestShooterWeaponBase.h"

void ATestShooterPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    check(InputComponent);
    InputComponent->BindAction("Crouch", IE_Pressed, this, &ATestShooterPlayerController::StartCrouch);
    InputComponent->BindAction("Crouch", IE_Released, this, &ATestShooterPlayerController::StopCrouch);
    InputComponent->BindAction("Aim", IE_Pressed, this, &ATestShooterPlayerController::StartAim);
    InputComponent->BindAction("Aim", IE_Released, this, &ATestShooterPlayerController::StopAim);
    InputComponent->BindAction("Jump", IE_Pressed, this, &ATestShooterPlayerController::Jump);
    InputComponent->BindAction("Jump", IE_Released, this, &ATestShooterPlayerController::StopJumping);
    InputComponent->BindAction("Fire", IE_Pressed, this, &ATestShooterPlayerController::Fire);
	
    InputComponent->BindAxis("MoveForwardBackward", this, &ATestShooterPlayerController::MoveForwardBackward);
    InputComponent->BindAxis("MoveRightLeft", this, &ATestShooterPlayerController::MoveRightLeft);
    InputComponent->BindAxis("Turn", this, &ATestShooterPlayerController::Turn);
    InputComponent->BindAxis("LookUp", this, &ATestShooterPlayerController::LookUp);

}

void ATestShooterPlayerController::BeginPlay()
{
    Super::BeginPlay();
    PossessedCharacter= Cast<ATestShooterCharacter>(GetPawn());
}


void ATestShooterPlayerController::MoveForwardBackward(float Value)
{
   //PossessedCharacter= Cast<ATestShooterCharacter>(GetPawn());
    if (PossessedCharacter && Value != 0.0f)
        PossessedCharacter->MoveForwardBackward(Value);
}

void ATestShooterPlayerController::MoveRightLeft(float Value)
{
    //PossessedCharacter= Cast<ATestShooterCharacter>(GetPawn());
    if (PossessedCharacter && Value != 0.0f)
        PossessedCharacter->MoveRightLeft(Value);
}

void ATestShooterPlayerController::StartCrouch()
{
    if(PossessedCharacter)
        PossessedCharacter->Crouch(false);
}

void ATestShooterPlayerController::StopCrouch()
{
    if(PossessedCharacter)
        PossessedCharacter->UnCrouch(false);
}

void ATestShooterPlayerController::StartAim()
{
    if(PossessedCharacter)
        PossessedCharacter->StartAimServer();
}

void ATestShooterPlayerController::StopAim()
{
    if(PossessedCharacter)
        PossessedCharacter->StopAimServer();
}

void ATestShooterPlayerController::Fire()
{
    if(PossessedCharacter && PossessedCharacter->GetEquippedWeapon())
        PossessedCharacter->GetEquippedWeapon()->ShootServer();
}

void ATestShooterPlayerController::Jump()
{
    if(PossessedCharacter)
        PossessedCharacter->Jump();
}

void ATestShooterPlayerController::StopJumping()
{
    if(PossessedCharacter)
        PossessedCharacter->StopJumping();
}

void ATestShooterPlayerController::Turn(float Val)
{
    if(PossessedCharacter)
       PossessedCharacter->AddControllerYawInput(Val);
}

void ATestShooterPlayerController::LookUp(float Val)
{
    if(PossessedCharacter)
    {
       PossessedCharacter->AddControllerPitchInput(Val);
       PossessedCharacter->SetAimOffsetServer();
    }
      
}
