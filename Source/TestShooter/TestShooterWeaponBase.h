// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


//#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "TestShooterProjectileBase.h"
#include "GameFramework/Actor.h"
#include "TestShooterWeaponBase.generated.h"

UCLASS()
class TESTSHOOTER_API ATestShooterWeaponBase : public AActor
{
	GENERATED_BODY()

protected:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector AimPosition;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
	class USkeletalMeshComponent* WeaponMesh;

	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
	class USceneComponent* Muzzle;*/
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= Weapon)
	TSubclassOf<ATestShooterProjectileBase> ProjectileClass;

	// Sets default values for this actor's properties
	ATestShooterWeaponBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*UFUNCTION()
	FVector GetAimPosition();*/

	UFUNCTION(Server, Reliable)
    void ShootServer();

	/*UFUNCTION(NetMulticast, Reliable)
    void ShootMulticast();*/
};
