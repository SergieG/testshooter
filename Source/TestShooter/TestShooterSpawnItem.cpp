// Fill out your copyright notice in the Description page of Project Settings.


#include "TestShooterSpawnItem.h"

#include "TestShooterCharacter.h"


// Sets default values
ATestShooterSpawnItem::ATestShooterSpawnItem()
{
	SetReplicates(true);

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;	
	//OnActorBeginOverlap.AddDynamic(this, &ATestShooterSpawnItem::PickUp);

	ItemSceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = ItemSceneRoot;
}

// Called when the game starts or when spawned
void ATestShooterSpawnItem::BeginPlay()
{
	Super::BeginPlay();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Spawned"));
}

void ATestShooterSpawnItem::PickUp(AActor* MyOverlappedActor, AActor* OtherActor)
{
	//if(Cast<ATestShooterCharacter>(OtherActor))
		//Destroy();
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("PickUp"));
}


// Called every frame
void ATestShooterSpawnItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

