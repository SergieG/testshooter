// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TestShooterSpawnerBase.generated.h"

UCLASS()
class TESTSHOOTER_API ATestShooterSpawnerBase : public AActor
{
	GENERATED_BODY()

protected:

	UPROPERTY(VisibleDefaultsOnly)
	class USceneComponent* SceneRoot;

	UPROPERTY(VisibleDefaultsOnly)
	class UStaticMeshComponent* SpawnerMesh;
	
public:	
	// Sets default values for this actor's properties
	ATestShooterSpawnerBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
