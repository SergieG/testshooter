﻿#include "TestShooterEditor.h"

//#include "SpawnerTool.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"
//#include "TestShooter/Spawner.h"

DEFINE_LOG_CATEGORY(TestShooterEditor);

IMPLEMENT_GAME_MODULE(FTestShooterEditorModule, TestShooterEditor);

#define LOCTEXT_NAMESPACE "TestShooterEditor"

void FTestShooterEditorModule::StartupModule()
{
    UE_LOG(TestShooterEditor, Warning, TEXT("TestShooterEditor: Log Started"));
    //
    // //static FName PropertyEditor("PropertyEditor");
    // FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");
    // PropertyModule.RegisterCustomClassLayout(ASpawner::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&FSpawnerTool::MakeInstance));
}

void FTestShooterEditorModule::ShutdownModule()
{
    UE_LOG(TestShooterEditor, Warning, TEXT("TestShooterEditor: Log Ended"));

    // if (FModuleManager::Get().IsModuleLoaded("PropertyEditor"))
    // {
    //     FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>("PropertyEditor");
    //     PropertyModule.UnregisterCustomClassLayout(ASpawner::StaticClass()->GetFName());
    // }
}

#undef LOCTEXT_NAMESPACE