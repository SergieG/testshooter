// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TestShooterEditor : ModuleRules
{
	public TestShooterEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Slate", "UnrealEd" });
		
		PublicDependencyModuleNames.AddRange(new string[] { "TestShooter" });

		PrivateDependencyModuleNames.AddRange(new string[] {"PropertyEditor", "LevelEditor", "SlateCore"});

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
